let arr = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];

function createList(arr, selector=document.body) {
    
arr = arr.map(function callback(index) {
    return `<li>${ index }</li>`;
});

let list = `<ul>${arr.join('')}</ul>`;
selector.insertAdjacentHTML("beforeend",list)
};

createList(arr);

